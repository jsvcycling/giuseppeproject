#include <QtWidgets>

#include <sstream>
#include <fstream>

#include <cstdlib>

#ifndef QT_NO_PRINTER
    #include <QtPrintSupport/QPrintDialog>
#endif /* !QT_NO_PRINTER */

#include "mainwindow.h"

#define IMAGE_SIZE_X	97
#define IMAGE_SIZE_Y	56

/*
std::vector<std::string> SplitString(const std::string &str)
{
    std::istringstream buf(str);
    std::vector<std::string> out;

    std::copy(std::istream_iterator<std::string>(buf),
              std::istream_iterator<std::string>(),
              std::back_inserter<std::vector<std::string>>(out));

    return out;
}

QImage LoadScan(const std::string filename)
{
    QImage img;
    QVector<Pixel> data;

    FILE *file = fopen(filename.c_str(), "r");

    if (file)
    {
        QTextStream strm(file);

        strm.readLine();

        while (!strm.atEnd())
        {
            Pixel pxl;
            QString line = strm.readLine();
            std::vector<std::string> strs;

            strs = SplitString(line.toStdString());

            if (strs.size() == 0)
            {
                QMessageBox msgbox;

                msgbox.setText("String std::vector is empty.");
                msgbox.exec();
            }

            pxl.x = std::atoi(strs[0].c_str());
            pxl.y = std::atoi(strs[1].c_str());
            pxl.data = std::atof(strs[2].c_str());

            data.push_back(pxl);
        }
    }

    fclose(file);

    img = QImage(data.last().x, data.last().y, QImage::Format_RGB16);

    int id = 0;
    for (int i = 0; i < data.last().x; i++)
    {
        for (int j = 0; j < data.last().y; j++)
        {
            int colour = ((data[id].data * 255) / 20);

            img.setPixel(data[id].x, data[id].y, QRgb(colour));

            id++;
        }
    }

    return img;
}
*/

QImage LoadScan(const std::string filename)
{
    float **data;
    QImage img = QImage(IMAGE_SIZE_X, IMAGE_SIZE_Y, QImage::Format_RGB444);
    char buf[32];
    char buf3[32];
    int cont;
    QColor color;

    data = (float **)malloc(sizeof(float) * IMAGE_SIZE_X);

    for (int i = 0; i <= (IMAGE_SIZE_X - 1); i++)
    {
        data[i] = (float *)malloc(sizeof(float) * IMAGE_SIZE_Y);
    }

    // float temp;

    std::fstream filebuf;

    filebuf.open(filename.c_str());

    filebuf.getline(buf, 32);

    printf("Image Data\n");

    // Reiterate over each pixel, very inefficient, needs to be fixed.
    for (int x = 1; x <= IMAGE_SIZE_X; x++)
    {
        for (int y = 1; y <= IMAGE_SIZE_Y; y++)
        {
            filebuf.getline(buf, 32);

            // Only copy the values.
            cont = 0;
            for (int i = 8; i < 32; i++)
            {
                if (buf[i] != '\t' && buf[i] != ' ')
                {
                    buf3[cont] = buf[i];
                    cont += 1;
                }
            }

            // Set the pixel's value.
            // data[x - 1][y - 1] = (float)atof(buf3);

            int colour = (((float)atof(buf3) * 255) / 20);

            color = QColor(colour, colour, colour);

            img.setPixel(x - 1, y - 1, color.rgb());

            printf("%d %d = %f = %d\n", x - 1, y - 1, atof(buf3), colour);
        }
    }

    filebuf.close();

    return img;
}

MainWindow::MainWindow()
{
    mImageLabel = new QLabel;
    mImageLabel->setBackgroundRole(QPalette::Base);
    mImageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    mImageLabel->setScaledContents(true);

    mScrollArea = new QScrollArea;
    mScrollArea->setBackgroundRole(QPalette::Dark);
    mScrollArea->setWidget(mImageLabel);

    setCentralWidget(mScrollArea);

    CreateActions();
    CreateMenus();

    setWindowTitle((tr("MRI Viewer")));

    resize(1280, 720);
}

void MainWindow::Open()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open Scan"), QDir::currentPath(), tr("MRI Scans (*.dat)"));

    if (!filename.isEmpty())
    {
        QImage image = LoadScan(filename.toStdString());

        if (image.isNull())
        {
            QMessageBox::information(this, tr("Error"), tr("Cannot load %1.").arg(filename));

            return;
        }

        mImageLabel->setPixmap(QPixmap::fromImage(image));
        mScaleFactor = 1.0;

//        mPrintAction->setEnabled(true);
        mFitToWindowAction->setEnabled(true);

        UpdateActions();

        if (!mFitToWindowAction->isChecked())
        {
            mImageLabel->adjustSize();
        }
    }
}

//void MainWindow::PrintScan()
//{
//    Q_ASSERT(mImageLabel->pixmap());

//#if !defined(QT_NO_PRINTER) && !defined(QT_NO_PRINTDIALOG)
//    QPrintDialog dialog(&mPrinter, this);

//    if (dialog.exec())
//    {
//        QPainter painter(&mPrinter);
//        QRect rect = painter.viewport();
//        QSize size = mImageLabel->pixmap()->size();
//        size.scale(rect.size(), Qt::KeepAspectRatio);

//        painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
//        painter.setWindow(mImageLabel->pixmap()->rect());
//        painter.drawPixmap(0, 0, *mImageLabel->pixmap());
//    }
//#endif /* !QT_NO_PRINTER && ... */
//}

void MainWindow::Exit()
{

}

void MainWindow::ZoomIn()
{
    ScaleImage(1.25);
}

void MainWindow::ZoomOut()
{
    ScaleImage(0.8);
}

void MainWindow::NormalSize()
{
    mImageLabel->adjustSize();
    mScaleFactor = 1.0;
}

void MainWindow::FitToWindow()
{
    bool fit = mFitToWindowAction->isChecked();

    mScrollArea->setWidgetResizable(fit);

    if (!fit)
    {
        NormalSize();
    }

    UpdateActions();
}


void MainWindow::About()
{

}

void MainWindow::AboutQt()
{

}

void MainWindow::CreateActions()
{
    // Open Scan
    mOpenAction = new QAction(tr("&Open Scan..."), this);
    mOpenAction->setShortcut(tr("Ctrl+O"));
    connect(mOpenAction, SIGNAL(triggered()), this, SLOT(Open()));

//    // Print Image
//    mPrintAction = new QAction(tr("&Print Scan..."), this);
//    mPrintAction->setShortcut(tr("Ctrl+P"));
//    mPrintAction->setEnabled(false);
//    connect(mPrintAction, SIGNAL(triggered()), this, SLOT(PrintScan()));

    // Quit
    mQuitAction = new QAction(tr("E&xit"), this);
    mQuitAction->setShortcut(tr("Ctrl+Q"));
    connect(mQuitAction, SIGNAL(triggered()), this, SLOT(Exit()));

    // Zoom-In
    mZoomInAction = new QAction(tr("Zoom-&In (25%)"), this);
    mZoomInAction->setShortcut(tr("Ctrl++"));
    mZoomInAction->setEnabled(false);
    connect(mZoomInAction, SIGNAL(triggered()), this, SLOT(ZoomIn()));

    // Zoom-Out
    mZoomOutAction = new QAction(tr("Zoom-&Out (25%)"), this);
    mZoomOutAction->setShortcut(tr("Ctrl+-"));
    mZoomOutAction->setEnabled(false);
    connect(mZoomOutAction, SIGNAL(triggered()), this, SLOT(ZoomOut()));

    // Normal-Size
    mNormalSizeAction = new QAction(tr("&Normal Size"), this);
    mNormalSizeAction->setShortcut(tr("Ctrl+S"));
    mNormalSizeAction->setEnabled(false);
    connect(mNormalSizeAction, SIGNAL(triggered()), this, SLOT(NormalSize()));

    // Fit To Window
    mFitToWindowAction = new QAction(tr("&Fit to Window"), this);
    mFitToWindowAction->setShortcut(tr("Ctrl+F"));
    mFitToWindowAction->setEnabled(false);
    mFitToWindowAction->setCheckable(true);
    connect(mFitToWindowAction, SIGNAL(triggered()), this, SLOT(FitToWindow()));

    // About
    mAboutAction = new QAction(tr("&About MRI Viewer"), this);
    connect(mAboutAction, SIGNAL(triggered()), this, SLOT(About()));

    // AboutQt
    mAboutQtAction = new QAction(tr("About &Qt"), this);
    connect(mAboutQtAction, SIGNAL(triggered()), this, SLOT(AboutQt()));
}

void MainWindow::CreateMenus()
{
    // File Menu
    mFileMenu = new QMenu(tr("&File"), this);
    mFileMenu->addAction(mOpenAction);
//    mFileMenu->addAction(mPrintAction);
    mFileMenu->addSeparator();
    mFileMenu->addAction(mQuitAction);

    // View Menu
    mViewMenu = new QMenu(tr("&View"), this);
    mViewMenu->addAction(mZoomInAction);
    mViewMenu->addAction(mZoomOutAction);
    mViewMenu->addAction(mNormalSizeAction);
    mViewMenu->addSeparator();
    mViewMenu->addAction(mFitToWindowAction);

    // Help Menu
    mHelpMenu = new QMenu(tr("&Help"), this);
    mHelpMenu->addAction(mAboutAction);
    mHelpMenu->addAction(mAboutQtAction);

    menuBar()->addMenu(mFileMenu);
    menuBar()->addMenu(mViewMenu);
    menuBar()->addMenu(mHelpMenu);
}

void MainWindow::UpdateActions()
{
    mZoomInAction->setEnabled(!mFitToWindowAction->isChecked());
    mZoomOutAction->setEnabled(!mFitToWindowAction->isChecked());
    mNormalSizeAction->setEnabled(!mFitToWindowAction->isChecked());
}

void MainWindow::ScaleImage(double factor)
{
    Q_ASSERT(mImageLabel->pixmap());

    mScaleFactor *= factor;

    mImageLabel->resize(mScaleFactor * mImageLabel->pixmap()->size());

    AdjustScrollbar(mScrollArea->horizontalScrollBar(), factor);
    AdjustScrollbar(mScrollArea->verticalScrollBar(), factor);

    mZoomInAction->setEnabled(mScaleFactor < 3.0);
    mZoomOutAction->setEnabled(mScaleFactor > 0.333);
}

void MainWindow::AdjustScrollbar(QScrollBar *scrollbar, double factor)
{
    scrollbar->setValue(int(factor * scrollbar->value() + ((factor - 1) * scrollbar->pageStep() /2 )));
}
