#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

//#ifndef QT_NO_PRINTER
//    #include <QtPrintSupport/QPrinter>
//#endif /* !QT_NO_PRINTER */

class QAction;
class QLabel;
class QScrollArea;
class QMenu;
class QScrollBar;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow();

private slots:
    void Open();
//    void PrintScan();
    void Exit();
    void ZoomIn();
    void ZoomOut();
    void NormalSize();
    void FitToWindow();
    void About();
    void AboutQt();

private:
    void CreateActions();
    void CreateMenus();
    void UpdateActions();
    void ScaleImage(double factor);

    void AdjustScrollbar(QScrollBar *scrollbar, double factor);

    QLabel *mImageLabel;
    QScrollArea *mScrollArea;
    double mScaleFactor;

//#if !defined(QT_NO_PRINTER)
//    QPrinter mPrinter;
//#endif /* !QT_NO_PRINTER */

    QAction *mOpenAction;
//    QAction *mPrintAction;
    QAction *mQuitAction;
    QAction *mZoomInAction;
    QAction *mZoomOutAction;
    QAction *mNormalSizeAction;
    QAction *mFitToWindowAction;
    QAction *mAboutAction;
    QAction *mAboutQtAction;

    QMenu *mFileMenu;
    QMenu *mViewMenu;
    QMenu *mHelpMenu;
};

QImage LoadScan(const QString filename);

struct Pixel
{
    int x;
    int y;

    float data;
};

#endif // MAINWINDOW_H
